Chromeapp Trello è uno scorciatoia per lanciare [Trello] come se fosse una applicazione di sistema, in realtà l'app apre il sito di [Trello].

Il lavoro si ispira a questo articolo [creazione-chrome-app-from-url], ma esistono altre guide.

[Trello]:http://trello.com
[creazione-chrome-app-from-url]:http://www.howtogeek.com/169220/how-to-create-custom-chrome-web-app-shortcuts-for-your-favorite-websites/
[default-chrome-extension-folder-linux]:~/.config/google-chrome/Default/Extensions/whatsapp